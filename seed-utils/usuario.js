const bcrypt = require("bcrypt");

function randomUser() {

  var result = '';
  var masculinos = ['Fernando', 'Roberto', 'Kessyus', 'Ariel', 'Ailton', 'Wellington', 'Carlos', 'Adolfo', 'André', 'Virgínio', 'Felipe', 'Luis', 'Gabriel', 'Estefano', 'Elberth', 'Liniquer', 'Ezer']
  var femininos = ['Fernanda', 'Roberta', 'Júlia', 'Amanda', 'Lívia', 'Samanta', 'Luíza', 'Yasmim', 'Gabriela', 'Jéssica', 'Rúbia', 'Estela', 'Socorro', 'Suelen']
  var nomes = [masculinos, femininos]
  var indexNomes = Math.floor(Math.random() * nomes.length)
  var indexFilter = Math.floor(Math.random() * nomes[indexNomes].length)
  return nomes[indexNomes][indexFilter];
}

function randomNomeCompleto(user) {
  var result = '';
  var primeironome = ''
  var normais = ['Paes', 'Costa', 'Fernandes', 'Lisboa', 'Carneiro', 'Misericórdia', 'Andrade', 'Barbosa', 'Barros', 'Batista', 'Borges', 'Campos', 'Cardoso', 'Carvalho', 'Dias', 'Freitas', 'Ferreira']
  var complementos = ['da Nóbrega', 'da Silva', 'da Paz', 'dos Santos', 'da Catatuaba', 'dos Dias', 'de Jesus', 'de Souza', 'do Nascimento']
  var nomes = [normais, complementos]
  var tam = Math.floor(Math.random() * (3 - 2 + 1)) + 2;
  for (var i = 0; i < tam; i++) {
    var indexnormal = Math.floor(Math.random() * nomes[0].length)
    primeironome += nomes[0][indexnormal] + ' '
    nomes[0].splice(indexnormal, 1);
  }
  var isComplemented = Math.floor(Math.random() * (1 - 0 + 1)) + 0;

  var indexcomplemento = Math.floor(Math.random() * nomes[1].length)
  if (isComplemented == 1) {
    result = primeironome + nomes[1][indexcomplemento]
  } else {
    result = primeironome.slice(0, -1)
  }
  return user + ' ' + result;
}
module.exports = {
  up: async (queryInterface, Sequelize) => {

    var usuarios = []
    for (var i = 1; i < 50; i++) {
      var user = randomUser()
      usuarios.push({
        username: user.toLocaleLowerCase(),
        cpf: '00000000',
        senha: await bcrypt.hash('123', 3),
        nome: randomNomeCompleto(user),
        email: user.toLocaleLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "") + '@example.com',
        datanascimento: new Date(),
        createdAt: new Date(),
        updatedAt: new Date()
      })

    }
    await queryInterface.bulkInsert('usuario', usuarios)

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('usuario', null, {});
  }
};

