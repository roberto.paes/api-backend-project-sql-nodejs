function randomCargo() {
    var estados = [{id:0,descricao:'Usuário comum',cargo:'Usuário comum',tipo:0},{id:1,descricao:'Estacionamento Owner',cargo:'Dono de Estacionamento',tipo:1}, {id:2,descricao:'Administrador geral',cargo:'Administrador de sistemas',tipo:2}]
   
  return estados
  }
  
  
  module.exports = {
    up: async (queryInterface, Sequelize) => {
  
      var cargos = []
  
      for (var i = 0; i < randomCargo().length; i++) {
        cargos.push({
          id: randomCargo()[i].id,
          descricao: randomCargo()[i].descricao,
          cargo: randomCargo()[i].cargo
        })
      }
      await queryInterface.bulkInsert('cargo', cargos)
  
    },
  
    down: async (queryInterface, Sequelize) => {
      /**
       * Add commands to revert seed here.
       *
       * Example:
       * await queryInterface.bulkDelete('People', null, {});
       */
      await queryInterface.bulkDelete('cargo', null, {});
    }
  };
  
  
  