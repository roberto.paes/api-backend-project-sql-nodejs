function RandomPlaca() {

  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var numbers = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
  var charactersLength = characters.length;
  var numberssLength = numbers.length;
  for (var i = 0; i < 3; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  result += '-'
  for (var i = 0; i < 4; i++) {
    result += numbers.charAt(Math.floor(Math.random() * numberssLength));
  }
  return result;

}
function RandomMarca() {

  var result = '';
  var marcas = ['Fiat', 'Toyota', 'Honda', 'Nissan', 'Chevrolet', 'Hyundai', 'Kia']


  result = marcas[Math.floor(Math.random() * marcas.length)];

  return result;

}


module.exports = {
  up: async (queryInterface, Sequelize) => {

    var carros = []
    const usuario = await queryInterface.sequelize.query(`SELECT id AS usercounts from usuario`)

    for (var i = 1; i < usuario[0].length; i++) {

      var user = usuario[0][i].usercounts

      carros.push({
        marca: RandomMarca(),
        modelo: 'examplemodelo' + user,
        placa: RandomPlaca(),
        createdAt: new Date(),
        updatedAt: new Date(),
        usuarioId: user
      })
    }
    await queryInterface.bulkInsert('carro', carros)

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('carro', null, {});
  }
};


