



function getRandomArbitrary(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
  
  
}
function randomEstacionamento() {
  
  var result = '';
  var masculinos = ['Fernando', 'Roberto', 'Kessyus', 'Ariel', 'Ailton', 'Wellington', 'Carlos', 'Adolfo', 'André', 'Virgínio', 'Felipe', 'Luis', 'Gabriel', 'Estefano', 'Elberth', 'Liniquer', 'Ezer']
  var femininos = ['Fernanda', 'Roberta', 'Júlia', 'Amanda', 'Lívia', 'Samanta', 'Luíza', 'Yasmim', 'Gabriela', 'Jéssica', 'Rúbia', 'Estela', 'Socorro', 'Suelen']
  var nomes = [masculinos, femininos]
  var indexNomes = Math.floor(Math.random() * nomes.length)
  var indexFilter = Math.floor(Math.random() * nomes[indexNomes].length)
  if (indexNomes == 0) {
    result = 'Estacionamento do ' + nomes[indexNomes][indexFilter];
  } else {
    result = 'Estacionamento da ' + nomes[indexNomes][indexFilter];
  }
  return result;
}
function randomRua() {
  var result = '';
  var marcas = ['Sargento', 'Alfândega', 'Álvaro', 'Chaves', 'Belmira', 'Buenos', 'Cruz', 'Edgard', 'Bela', 'Vista']
  var tam = Math.floor(Math.random() * (5 - 2 + 2)) + 2;
  for (var i = 0; i < tam; i++) {
    var random = Math.floor(Math.random() * marcas.length)
    result += marcas[random] + ' '
    marcas.splice(random, 1);
  }
  return result.slice(0, -1)
}
function randomBairro(municipio) {
  var result = '';
  var RioDeJaneiro = ['Barra da Tijuca', 'São Cristovão', 'Pavuna']//['Barra da Tijuca', 'São Cristovão', 'Pavuna', 'Cascadura', 'Madureira', 'Leblon', 'Copacabana']
  var RioSaoJoao = ['Vilar Dos Teles', 'Tiradentes']
  var sp = ['Higienópolis', 'Sumaré']
  switch(municipio){
    case 'Rio de Janeiro':
    result = RioDeJaneiro[Math.floor(Math.random() * RioDeJaneiro.length)];
    break
    case 'São João de Meriti':
    result = RioSaoJoao[Math.floor(Math.random() * RioSaoJoao.length)];
    break
    case 'Grande São Paulo':
    result = sp[Math.floor(Math.random() * sp.length)];
    break
  }
  return result;
}
function randomEstado() {
  var estados = ['RJ', 'SP']
  result = estados[Math.floor(Math.random() * estados.length)];
  return result
}
function randomMunicipio(estado) {
  var municipioRJ = ['Rio de Janeiro','São João de Meriti']
  var municipioSP = ['Grande São Paulo']
  switch(estado){
    case 'RJ':
    result = municipioRJ[Math.floor(Math.random() * municipioRJ.length)];
    break
    case 'SP':
    result = municipioSP[Math.floor(Math.random() * municipioSP.length)];
    break
  }
  return result
}
function randomCep() {
  
  var result = '';
  var numbers = '123456789'
  var numberssLength = numbers.length;
  
  for (var i = 0; i < 8; i++) {
    result += numbers.charAt(Math.floor(Math.random() * numberssLength));
  }
  return result;
  
}
function randomNum(min, max,options = false) {
  min = Math.ceil(min);
  max = Math.floor(max);
  var random = Math.random() * (max - min + 1) + min;
  var result 
  if(!options){
    result = Math.round(random * 100) / 100
  }else{
    result = Math.round(random) 
  }
  return result
}
module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    
    var estacionamentos = []
    
    const usuario = await queryInterface.sequelize.query(`SELECT id,nome from usuario`)
    for (var i = 1; i < usuario[0].length; i++) {
      var estado = randomEstado()
      var cidade = randomMunicipio(estado)
      estacionamentos.push({
        vagas: getRandomArbitrary(5, 250),
        nome: 'Estacionamento de ' + usuario[0][i].nome.split(' ')[0] + ' ' + usuario[0][i].nome.split(' ').pop(),
        logradouro: randomRua(),
        usuarioId: usuario[0][i].id,
        estado: estado,
        cidade:cidade,
        numero:randomNum(30,255),
        bairro: randomBairro(cidade),
        cep: randomCep(),
        vaga_ocupada: 0
      })
    }
    await queryInterface.bulkInsert('estacionamento', estacionamentos)

    const estaciomanetosMount = await queryInterface.sequelize.query(`SELECT id from estacionamento`)
    var tarifas = []
    
    for (var i = 1; i < estaciomanetosMount[0].length; i++) {
      let tarifaMount = [
        {
          "id":0,
          "tempo":randomNum(10,15,true),
          "preco":randomNum(1,5)
        },
        {
          "id":1,
          "tempo":randomNum(16,30,true),
          "preco":randomNum(6,10)
        },
        {
          "id":2,
          "tempo":randomNum(31,45,true),
          "preco":randomNum(11,25)
        },
        {
          "id":3,
          "tempo":60,
          "preco":randomNum(26,32)
        },
        {
          "id":4,
          "tempo":"diaria",
          "preco":randomNum(0,50)
        }
      ]
      
      tarifas.push({tarifa:JSON.stringify(tarifaMount),estacionamentoId:estaciomanetosMount[0][i].id})
    }

    await queryInterface.bulkInsert('tarifa', tarifas)
    
  },
  
  down: async (queryInterface, Sequelize) => {
    /**
    * Add commands to revert seed here.
    *
    * Example:
    * await queryInterface.bulkDelete('People', null, {});
    */
    await queryInterface.bulkDelete('estacionamento', null, {});
  }
};

