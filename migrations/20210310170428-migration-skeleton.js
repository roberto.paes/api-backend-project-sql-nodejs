

const estacionamentoMigration = require("../migrations-utils/estacionamento_migration");
const usuarioMigration = require("../migrations-utils/usuario_migration");
const carroMigration = require("../migrations-utils/carro_migration");
const reservaMigration = require("../migrations-utils/reserva_migration");
const cargoMigration = require("../migrations-utils/cargo_migration");
const tarifaMigration = require("../migrations-utils/tarifa_migration");


module.exports = {
  up: async (queryInterface, Sequelize) => {
    await cargoMigration.up(queryInterface, Sequelize)
    await usuarioMigration.up(queryInterface, Sequelize)

    await estacionamentoMigration.up(queryInterface, Sequelize)
    await tarifaMigration.up(queryInterface, Sequelize)

    await carroMigration.up(queryInterface, Sequelize)
    await reservaMigration.up(queryInterface, Sequelize)

  },
  
  
  down: async (queryInterface, Sequelize) => {
    await cargoMigration.down(queryInterface, Sequelize)
    await usuarioMigration.down(queryInterface, Sequelize)
    await tarifaMigration.down(queryInterface, Sequelize)
    await estacionamentoMigration.down(queryInterface, Sequelize)
    await carroMigration.down(queryInterface, Sequelize)
    await reservaMigration.down(queryInterface, Sequelize)

  }
};
