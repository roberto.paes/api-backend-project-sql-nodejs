

module.exports = {
    up: async (queryInterface, Sequelize) => {
        
        await queryInterface.createTable('tarifa', {
            id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true
            },
            tarifa: {
                type: Sequelize.JSON,
                allowNull: false
            }
        });

        await queryInterface.addColumn('tarifa', 'estacionamentoId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'estacionamento',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
  
        })
    },
    
    down: async (queryInterface, Sequelize) => {
        /**
        * Add commands to revert seed here.
        *
        * Example:
        * await queryInterface.bulkDelete('People', null, {});
        */
        await queryInterface.bulkDelete('tarifa', null, {});
    }
};


