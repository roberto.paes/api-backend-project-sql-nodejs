
  
  module.exports = {
    up: async (queryInterface, Sequelize) => {
  
      await queryInterface.createTable('estacionamento', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        vagas: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        nome: {
          type: Sequelize.STRING,
          allowNull: false
        },
        cep: {
          type: Sequelize.STRING(10),
          allowNull: true
        },
        logradouro: {
          type: Sequelize.STRING(255),
          allowNull: true
        },
        numero: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        complemento: {
          type: Sequelize.STRING,
          defaultValue: '',
          allowNull: true
        },
        bairro: {
          type: Sequelize.STRING,
          allowNull: true
        },
        cidade: {
          type: Sequelize.STRING,
          allowNull: true
        },
        estado: {
          type: Sequelize.STRING(2),
          allowNull: true
        },
        vaga_ocupada: {
          type: Sequelize.INTEGER,
          defaultValue: 0,
          allowNull: true
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: new Date(),
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: new Date(),
        }
  
      });
      await queryInterface.addColumn('estacionamento', 'usuarioId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'usuario',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
  
        })
  
    },
  
    down: async (queryInterface, Sequelize) => {
      /**
       * Add commands to revert seed here.
       *
       * Example:
       * await queryInterface.bulkDelete('People', null, {});
       */
      await queryInterface.bulkDelete('estacionamento', null, {});
    }
  };
  
  
  