module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.createTable('carro', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      marca: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      modelo: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      placa: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: new Date(),
      }


    });
    await queryInterface.addColumn('carro', 'usuarioId',
    {
      type: Sequelize.INTEGER,
      references: {
        model: 'usuario',
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('carro', null, {});
  }
};

