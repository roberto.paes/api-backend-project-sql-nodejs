
  
  module.exports = {
    up: async (queryInterface, Sequelize) => {
  
        await queryInterface.createTable('cargo', {
            id: {
              type: Sequelize.INTEGER,
              autoIncrement: false,
              allowNull: false,
              primaryKey: true
            },
            descricao: {
              type: Sequelize.STRING,
              allowNull: true
            },
            cargo: {
              type: Sequelize.STRING,
              allowNull: true
            }
        })
    
    },
  
    down: async (queryInterface, Sequelize) => {
      /**
       * Add commands to revert seed here.
       *
       * Example:
       * await queryInterface.bulkDelete('People', null, {});
       */
      await queryInterface.bulkDelete('cargo', null, {});
    }
  };
  
  
  