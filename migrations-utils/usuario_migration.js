
  
  module.exports = {
    up: async (queryInterface, Sequelize) => {
  
        await queryInterface.createTable('usuario', {
            id: {
              type: Sequelize.INTEGER,
              autoIncrement: true,
              allowNull: false,
              primaryKey: true
            },
            username: {
              type: Sequelize.TEXT,
              allowNull: false
            },
            senha: {
              type: Sequelize.TEXT,
              allowNull: true
            },
            nome: {
              type: Sequelize.TEXT,
              allowNull: false
            },
            email: {
              type: Sequelize.TEXT,
              allowNull: false
            },
            cpf: {
              type: Sequelize.TEXT,
              allowNull: false
            },
            datanascimento: {
              type: Sequelize.DATEONLY,
              allowNull: false
            },
            createdAt: {
              allowNull: false,
              type: Sequelize.DATE,
              defaultValue: new Date(),
            },
            updatedAt: {
              allowNull: false,
              type: Sequelize.DATE,
              defaultValue: new Date(),
            }
      
          });
          await queryInterface.addColumn('usuario', 'tipo',
          {
            type: Sequelize.INTEGER,
            references: {
              model: 'cargo',
              key: 'id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
      
          })
    },
  
    down: async (queryInterface, Sequelize) => {
      /**
       * Add commands to revert seed here.
       *
       * Example:
       * await queryInterface.bulkDelete('People', null, {});
       */
      await queryInterface.bulkDelete('usuario', null, {});
    }
  };
  
  
  