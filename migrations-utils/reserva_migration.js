
  
  module.exports = {
    up: async (queryInterface, Sequelize) => {
  
      await queryInterface.createTable('reserva', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
  
        nome: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        entrada: {
          type: Sequelize.DATE,
          allowNull: true
        },
        saida: {
          type: Sequelize.DATE,
          allowNull: true
        },
        preco: {
          type: Sequelize.DECIMAL(8, 2),
          allowNull: true
        },
        status: {
          type: Sequelize.INTEGER,
          defaultValue: 1,
          allowNull: true
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: new Date(),
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: new Date(),
        }
  
  
      });
  
      await queryInterface.addColumn('reserva', 'usuarioId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'usuario',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        })
      await queryInterface.addColumn('reserva', 'carroId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'carro',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
  
        })
      await queryInterface.addColumn('reserva', 'estacionamentoId',
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'estacionamento',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
  
        })
  
    },
  
    down: async (queryInterface, Sequelize) => {
      /**
       * Add commands to revert seed here.
       *
       * Example:
       * await queryInterface.bulkDelete('People', null, {});
       */
      await queryInterface.bulkDelete('reserva', null, {});
    }
  };
  
  
  