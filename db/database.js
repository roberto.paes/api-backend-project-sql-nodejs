require('dotenv').config();

var Sequelize = require('sequelize')
var sequelize = new Sequelize(process.env.MYSQL_DATABASE,process.env.MYSQL_USER,process.env.MYSQL_PASSWORD,{  
    host: process.env.MYSQL_HOST_IP,
    define: {
        freezeTableName: true
      },
dialect: 'mysql'
   })
   var db = {};

   db.Sequelize = Sequelize;
   db.sequelize = sequelize;

module.exports = db