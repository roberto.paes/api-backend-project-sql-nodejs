

const estacionamentoSeed = require("../seed-utils/estacionamento");
const usuarioSeed = require("../seed-utils/usuario");
const carroSeed = require("../seed-utils/carro");
const reservaSeed = require("../seed-utils/reserva");
const cargoSeed = require("../seed-utils/cargo");


function getRandomArbitrary(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;


}

const quantidade = 30;
module.exports = {
  up: async (queryInterface, Sequelize) => {

    await cargoSeed.up(queryInterface, Sequelize)
    await usuarioSeed.up(queryInterface, Sequelize)
    await estacionamentoSeed.up(queryInterface, Sequelize)
    await carroSeed.up(queryInterface, Sequelize)
    await reservaSeed.up(queryInterface, Sequelize)
   


  },

  down: async (queryInterface, Sequelize) => {


    await usuarioSeed.down(queryInterface, Sequelize)
    await estacionamentoSeed.down(queryInterface, Sequelize)
    await carroSeed.down(queryInterface, Sequelize)
    await reservaSeed.down(queryInterface, Sequelize)
    await cargoSeed.down(queryInterface, Sequelize)


  }
};
