const database = require('../../../db/database')
const tarifaUtil = require('../../Utils/tarifa')

const getAllres = async (req,res,next) =>{
  console.log('get all reserva res called')
  
  try{
    var resposta = await database.reserva.findAll({
      include: [
        { 
          model: database.estacionamento
        }
      ],
      where : { usuarioId:req.userId }
    })
    res.status(200).send(resposta)
    
    //  console.log(JSON.stringify(resposta, null, 2));
  }catch(Exception){
    res.status(404).send(Exception)
  }
  
}
const delresById = async (req,res,next) =>{
  console.log('del res called')
  var response = {}
  try{
    
    await database.reserva.findOne({where: {...req.body,usuarioId:req.userId}}).then((record) =>{
      if (!record) {
        throw ('not found.. we go to catch')
      }
      record.destroy()
      response.status = true
      response.error = 'Sucess.'
      res.status(201).json(response)
      
    })
  }
  catch(Exception){
    response.status = false
    response.error = 'Error on this request.'
    res.status(404).json(response)
  }
}
const getresById = async (req,res,next) =>{
  console.log('get res by id called')
  var response = {}
  try{
    var result = await database.reserva.findOne({
      where: { usuarioId:req.userId,id:req.params.id },
      include: [
        { 
          model: database.estacionamento 
        }
      ]
    })
    if(response == null){
      response.status = false
      response.error = 'Not found.'
      throw(response)
    }
    
    res.status(200).json(result)
  }catch(Exception){
    res.status(404).json(Exception)
  }
  
}
const update= async (req,res,next) =>{
  var response = {}
  try{
    const body = req.body.find
    delete body.usuarioId
    console.log('body',body)
    var record = await database.reserva.findOne({where: { usuarioId:req.userId,...body}})
    
    console.log('record',record)
    if(record == null){
      response.status = false
      response.error = 'Not found.'
      throw(response)
    }
    var update = await record.update(req.body.update)
    if(update){
      response.status = true
      response.error = 'Sucess.'
      
    }else{
      response.status = false
      response.error = 'Not found.'
      throw(response)
    }
    
    res.status(200).json(response)
    
  }catch(Exception){
    res.status(404).json(Exception)
  }
}
const insert= async (req,res,next) =>{
  console.log('reserva called')
  var response = {}
  
  try{
    if(!req.body.carroId){
      throw("This request needs carro id.")
    }
    var estacionamento = await database.estacionamento.findOne({
      
      include: [
        { 
          model: database.tarifa,
        }, 
      ],
      where: { 'id': req.body.estacionamentoId} })
      if(!estacionamento){
        throw("This needs a parking id.")
      }
      
      if(estacionamento.usuarioId === req.userId){
    
        throw('Você está tentando efetuar uma reserva no seu próprio estacionamento.')
 
      }
      
      await database.reserva.create({...req.body,usuarioId:req.userId,preco:tarifaUtil.InitCalc(estacionamento.tarifa.tarifa,req.body.entrada,req.body.saida)}).catch((e)=>{
        
        throw("This request has a error.")
        
      })
      
      response.status = true
      response.error = 'Sucess request'
      res.status(200).json(response)
    }catch(Exception){
      response.status = false
      response.error = Exception
      res.status(406).json(response)
    }
  }
  
  const select = async (req,res,next ) =>{
    var response = {}
    try{
      await database.reserva.findAll( { 
        include: [
          { 
            model: database.estacionamento,
          }, 
          {
            model: database.carro, 
          }
        ]
        ,where: req.body,usuarioId:req.userId}).then((t)=>{
          if(Object.keys(t).length === 0){
            response.status = false
            response.error = 'Not found.'
          }else{
            res.status(200).json(t)
          }
        }).catch(e=>{
          response.status = false
          response.error = 'This request has a error.'
          throw(response)
        })
      }catch(Exception){
        
        response.status = false
        response.error = Exception
        res.status(404).json(response)
      }
      
    }
    module.exports = {
      delresById,
      getAllres,
      update,
      select,
      insert,
      getresById
    }