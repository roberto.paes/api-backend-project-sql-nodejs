const database = require('./../../db/database')
const carService = require('./../../services/car.service')
const getAllCarro = async (req,res,next) =>{
  console.log('get all carro called')
  try{
    
    var resposta = await database.carro.findAll({where : { 'usuarioId' : req.userId }})
    console.log(JSON.stringify(resposta, null, 2));
  }catch(Exception){
    console.log(Exception)
  }finally{
    res.status(200).send(resposta)
    
  }
}
//  res.status(404).json({error:'alguma coisa deu ruim' + Exception})





const delcarroById = async (req,res,next) =>{
  console.log('del carro called')
  console.log('received:',req.body)
  try{
    await database.carro.findOne({where: req.body}).then(record =>{
      if (!record) {
        throw('not found.. we go to catch')
      }
      record.destroy().then((deleted) => {
        console.log('deleted..'  + deleted.id)
        res.status(200).json(true)
      })
    })
  }
  catch(Exception){
    res.status(404).json(Exception)
  }
}
const getcarroById = async (req,res,next) =>{
  console.log('get carro by id called')
  try{
    var Userult = await database.carro.findOne({
      where : { 'id' : req.params.id, 'usuarioId' : req.userId  } 
    }
    )
    if (!Userult) {
      throw new Error('not found.. we go to catch')
    }
    res.status(200).json(Userult)
  }catch(Exception){
    res.status(404).json(false)
    
  }
}
const update= async (req,res,next) =>{
  var response = {}
  try{
    const body = req.body.find
    const user = req.userId
    var record = await database.carro.findOne({where: { usuarioId:req.userId,...body}})
    
    console.log('record',record)
    if(record == null){
      response.status = false
      response.error = 'Not found.'
      throw(response)
    }
    var update = await record.update(req.body.update)
    if(update){
      response.status = true
      response.error = 'Sucess'
    }else{
      response.status = false
      response.error = 'Not found'
      throw(response)
    }
    res.status(200).json(response)
    
  }catch(Exception){
    res.status(404).json(response)
  }
}
const insert= async (req,res,next) =>{
  var json = req.body
  json.usuarioId = req.userId
  try{
    if(await carService.buscarPorCarro(req.body.placa)){
      console.log('carro já cadastrado')
      throw("Esse carro já está cadastrado.")
    }
    
    await database.carro.create(json).then(insert => {
      if(!insert){
        throw new Error('error on insert')
      }
      res.status(200).send(true)
    })
  }catch(Exception){
    res.status(404).json({error: [...Exception]})
    
  }
}


const select = async (req,res,next ) =>{
  try{
    await database.carro.findAll({where: req.body})
    .then(record => {
      
      if (!record) {
        throw new Error('not found.. we go to catch')
      }
      
      res.status(200).json(record)
      
      
    })
  }catch(Exception){
    
    res.status(404).json(Exception)
  }
  
}
const login = async (req,res,next ) =>{
  var json = {}
  
  try{
    console.log('chamando login..' + req.body)
    await database.carro.findOne({include: [
      { 
        model: database.carro,
        
      }, 
      {
        model: database.reserva, 
      }
    ],where: req.body})
    .then(record => {
      
      if (!record) {
        throw new Error('not found.. we go to catch')
      }
      console.log(record)
      json.id = record.id
      json.username = record.username
      json.nome = record.nome
      json.email = record.email
      json.tipo = record.tipo
      json.carros = record.carros
      json.rseservas = record.reservas
      res.status(201).json(json)
      
      
    })
  }catch(Exception){
    json.status = false
    res.status(404).json(json)
  }
  
}
module.exports = {
  delcarroById,
  getAllCarro,
  update,
  select,
  login,
  insert,
  getcarroById
}