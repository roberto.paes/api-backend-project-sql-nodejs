const { ConsoleTransportOptions } = require('winston/lib/winston/transports')
const database = require('./../../db/database')
const googleService = require('../../services/google.service')


const searchDistance = async (req,res,next) =>{
  console.log('get search distance')
  const finderState = await googleService.googletGetInfoById(req.params.id)

  var estado = finderState.estado
  try{
  var result = await database.estacionamento.findAll({include: [
    { 
      model: database.tarifa,
    }, 
    { 
      model: database.reserva,
    }
  ],where:{estado}})
  if(!result){
    throw("Not find any parking..")
  }
  
 


  var googleResponse = await googleService.googleDistanceMatrix(req.params.id,result)
 console.log('google',JSON.stringify(googleResponse.data))
  var distances = googleResponse.data['rows'][0]['elements'].map((item,index)=>{
    //console.log(item['distance']['value'])
    if(item['status'] != 'OK'){
return 
    }
    var dista = item['distance']['value']
    var estacionamento = result[index]

    return {...estacionamento['dataValues'],dista}
    })
    var filtered = distances.filter(function (el) {
      return el != null;
    });
    filtered.sort(
      function(a, b) {
          return a.dista - b.dista
      }
  );
  console.log("#####TEST", JSON.stringify(distances))
 res.status(200).json(filtered)
}catch(Exception){
  res.status(200).json({})
}

}
const getAllEstUser = async (req,res,next) =>{
  console.log('get all est called')
  var result = await database.estacionamento.findAll({include: [
    { 
      model: database.tarifa,
    }, 
    { 
      model: database.reserva,
    }
  ],where : { 'usuarioId' : req.userId }})
  res.status(200).send(result)
}
const getAll = async (req,res,next) =>{
  console.log('get all est called')
  var result = await database.estacionamento.findAll({include: [
    { 
      model: database.tarifa
    }, 
    { 
      model: database.reserva
    }
  ]})

  res.status(200).json(result)
}

const getEstByBairroAndEstado = async (req,res,next) =>{
  var estado = req.params.estado
  var bairro = req.params.bairro
  var cidade = req.params.cidade
  console.log('body received',estado,cidade,bairro)
  var json = {}

  if(!cidade && !bairro){
    json  = {estado}
  }else if (!bairro){
    json  = {cidade,estado}
  }else{
    json = {bairro,estado,cidade}
  }
  console.log('get all bairro and estado called',estado,bairro)
  var result = await database.estacionamento.findAll({include: [
    { 
      model: database.tarifa,
    }], where:json})
  res.status(200).send(result)
}

const delEstById = async (req,res,next) =>{
  var response = {}
  try{
    await database.estacionamento.findOne({where: {...req.body,usuarioId:req.userId}}).then((record) =>{
      if (!record) {
        throw ('not found.. we go to catch')
      }
      record.destroy()
      response.status = true
      response.error = 'Sucess.'
      res.status(201).json(response)
      
    })
  }
  catch(Exception){
    response.status = false
    response.error = 'Error on this request.'
    res.status(404).json(response)
  }
}
const getestById = async (req,res,next) =>{
  console.log('get est by id called')
  var result = await database.estacionamento.findOne({include: [
    { 
      model: database.tarifa,
    }, 
  ],
  where : { 'id' : req.params.id} 
}
)
res.status(200).send(result)
}
const update= async (req,res,next) =>{
  var response = {}
  var finder = {...req.body.find}
  var updateFinder  = {...req.body.update}
  delete finder['usuarioId']
  try{
    
    var record = await database.estacionamento.findOne({where: { usuarioId:req.userId,...finder}})
    if(record == null){
      response.status = false
      response.error = 'Not found.'
      throw(response)
    }
    var update = await record.update(updateFinder)
    if(update){
      response.status = true
      response.error = 'Sucess.'
    }else{
      response.status = false
      response.error = 'Not found.'
      throw(response)
    }
    var updateTarifa = await database.tarifa.update({tarifa:updateFinder.tarifa},{where: { estacionamentoId:record.id}})
    if(updateTarifa){
      response.status = true
      response.error = 'Sucess.'
    }else{
      response.status = false
      response.error = 'Not found.'
      throw(response)
    }
    res.status(200).json(response)
    
  }catch(Exception){
    response.status = false
    response.error = Exception
    res.status(404).json(response)
  }
}
const insert= async (req,res,next) =>{
  var json = {...req.body}
  json.usuarioId = req.userId
  json.vaga_ocupada = 0
  delete json['tarifa']
  console.log('console log',json)
  try{
    await database.estacionamento.create(json).then(async (insert) => {
      console.log('#####TARIFa',req.body.tarifa)
      await database.tarifa.create({tarifa:req.body.tarifa,estacionamentoId:insert.id})
      if(!insert){
        throw('error on insert')
      }
      res.status(200).send(true)
    })
  }catch(Exception){
    res.status(404).json(Exception)
  }
}
const bairro = async (req,res,next) =>{
  try{
    database.estacionamento.findAll({
      attributes: ['bairro'],
      group: ['bairro']
    }).then(projects => 
      projects.map(project => project.bairro
        )).then(result => {
          res.status(200).send(result)
        })     
      }catch(Exception){
        res.status(404).send('not found any cities')
      }
    }
    const estado = async (req,res,next) =>{
      try{
        database.estacionamento.findAll({
          attributes: ['estado'],
          group: ['estado']
        }).then(projects => 
          projects.map(project => project.estado
            )).then(result => {
              res.status(200).send(result)
            })     
          }catch(Exception){
            res.status(404).send('not found any cities')
          }
        }
        const estacionamentoLista = async (req,res,next) =>{
          console.log('est lista chamado')
          try{
            var estados = []
            var bairros = []
            var cidades = []
            estados = await database.estacionamento.findAll({
              attributes: ['estado'],
              group: ['estado']
            }).then(projects => 
              projects.map(project => project.estado
                ))  
                var encontrado = []
                for(var i = 0; i < estados.length; i++){
                  
                  cidades = await database.estacionamento.findAll({
                    attributes: ['cidade'],
                    group: ['cidade'],
                    where: {estado:estados[i]}
                  }).then(projects => 
                    projects.map(project => project.cidade
                      ))  
                      
                      for(var c =0 ; c < cidades.length;c++){
                        bairros = await database.estacionamento.findAll({
                          attributes: ['bairro'],
                          group: ['bairro'],
                          where: {cidade:cidades[c]}
                        }).then(projects => 
                          projects.map(project => project.bairro
                            ))  
                            
                            var municipio = {municipio:cidades[c]}
                            
                            cidades[c] = {...municipio,bairros}
                            
                          }
                          
                          encontrado.push({estado:estados[i],cidades})
                        }
                        
                        
                        
                        
                        
                        
                        console.log('estacionamentos',encontrado)
                        res.status(200).json(encontrado) 
                      }catch(Exception){
                        res.status(404).json(encontrado)
                      }
                    }
                    const getByBairro = async (req,res,next) =>{
                      console.log('get est by bairro')
                      var result = await database.estacionamento.findAll({where: {bairro:req.params.bairro}})
                      res.status(200).send(result)
                    }
                    const getByEstado = async (req,res,next) =>{
                      console.log('get est by estado')
                      var result = await database.estacionamento.findAll({where: {estado:req.params.estado}})
                      res.status(200).send(result)
                    }
                    
                    
                    const search = async (req,res,next) =>{
                      try{
                        const Op = database.Sequelize.Op;
                        const query = `${req.params.query}%`;
                        const test = req.params.column
                        var result=  await database.estacionamento.findAll({ where: {[test]: { [Op.like]: query } } })     
                        res.status(200).json(result)
                      }catch(Exception){
                        res.status(404).json(false)
                      }
                    }
                    module.exports = {
                      delEstById,
                      search,
                      getAllEstUser,
                      getAll,
                      getByEstado,
                      estado,
                      getEstByBairroAndEstado,
                      getByBairro,
                      estacionamentoLista,
                      insert,
                      update,
                      searchDistance,
                      bairro,
                      getestById
                    }