const database = require('./../../db/database')
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");
const userService = require("../../services/user.service")

const getAllUser = async (req,res,next) =>{
    console.log('get all User called')
   try{
 var resposta = await database.user.findAll({
  include: [
    { 
      model: database.carro,
      include: [
            {
                model: database.reserva, 
            }]
    }
  ]
    })
    console.log(JSON.stringify(resposta, null, 2));
  }catch(Exception){
console.log(Exception)
  }finally{
    res.status(200).send(resposta)

  }
}
const delUserById = async (req,res,next) =>{
  
    try{
    await database.user.findOne({where: req.userId}).then(record =>{
 if (!record) {
     throw new Error('not found.. we go to catch')
   }
  record.destroy().then(deleted => {
     console.log('deleted..'  + deleted)
  res.status(200).json(true)
    })
 })
    }
catch(Exception){
    res.status(404).json(false)
}
}
const getUserById = async (req,res,next) =>{
    console.log('get User by id called')
    try{
    var Userult = await database.user.findOne(  {include: [
      { 
        model: database.cargo,
      }, 
      { 
        model: database.carro,
      }, 
       {
        model: database.reserva, 
        include: [
          {
              model: database.estacionamento, 
          }]
    }

    ],
    where : { 'id' : req.userId } 
  }
    )
    if (!Userult) {
      throw new Error('not found.. we go to catch')
    }
    var copyOfResult = {...Userult['dataValues']}
    const JsonCorrection = (label) => {
      var acceptables = ['senha']
      if(acceptables.includes(label)){
        delete copyOfResult[label]
      } 
    }
    Object.keys(copyOfResult).some((item) => JsonCorrection(item))

    res.status(200).json(copyOfResult)


}catch(Exception){
  res.status(404).json(false)

}
}
const update= async (req,res,next) =>{
try{
    await database.user.findOne({where: req.userId})
    .then(record => {
      
      if (!record) {
        throw new Error('not found.. we go to catch')
      }
      record.update(req.body.update).then( updatedRecord => {
        console.log(`updated record ${JSON.stringify(updatedRecord,null,2)}`)
        res.status(200).json(true)
      })

})
}catch(Exception){
    res.status(404).json(false)
}
}
const insert= async (req,res,next) =>{
  var json = {} 
  try{
   
      var usertipo = req.body.tipo ? req.body.tipo : 0
 if(await userService.buscarPorEmail(req.body.email)){
  throw("Esse email já está sendo usado.")
     }
     if(await userService.buscarPorUsuario(req.body.username)){
     throw("Esse nome de usuário já está sendo usado.")
        }
     if(await userService.buscarPorCpf(req.body.cpf)){
     throw("Você já possui um cadastro.")
        }
     await database.user.create({...req.body,tipo:usertipo}).then((insert) => {
    if(!insert){
        throw("Ops.. ocorreu um erro inesperado..")
    }
    json.status = true
    json.error = 'Sucess.'
     res.status(200).json(json)
    })
        }catch(Exception){
          json.status = false
          json.error = Exception
          console.log(json)
            res.status(406).json(json)
        }

        }
        const select = async (req,res,next ) =>{
          try{
            await database.user.findAll({include: [
              { 
                model: database.carro,
  
              }, 
               {
                model: database.reserva, 
            }
            ],where: req.body})
            .then(record => {
              if (!record) {
                throw new Error('not found.. we go to catch')
              }
                res.status(200).json(record)
        })
        }catch(Exception){
            res.status(404).json(Exception)
        }
        }        
        const login = async (req,res,next ) =>{
          var json = {}
var find
          try{
      
       
       if(req.body.email){
        find = {email:req.body.email}
       }
       else if(req.body.username){
        find = {username:req.body.username}
       }
            await database.user.findOne({
            where: {
              ...find,
            }})
            .then((record) => {
              if (!record) {
                throw('not found.. we go to catch')
              }
              else if(!bcrypt.compareSync(req.body.senha,record.senha)){
                throw('not found.. we go to catch')
              }
            const id = record.id 
            const tipo = record.tipo 
              const token = jwt.sign({id,tipo} , process.env.SECRET, {
                expiresIn: 900 // expires in 15 min
              });
     res.status(201).json({ auth: true, token: token });
     
        console.log('user logged ' + id)
        })
        }catch(Exception){
          json.status = false
          console.log(Exception)
          json.exception = Exception
            res.status(404).json(json)
        }
        }
module.exports = {
    delUserById,
    getAllUser,
    update,
    select,
    login,
    insert,
    getUserById
}