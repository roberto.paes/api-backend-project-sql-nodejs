const { Router } = require('express')
const reserva = require('./v1/reserva')
const estacionamento = require('./v1/estacionamento')
const user = require('./v1/user')
const test = require('./v2/test')

const { name , version } = require('./../package.json')
const carro = require('./v1/carro')

module.exports = (app) =>{
    const v1 = Router()
    const v2 = Router()
    v1.route('/').get((req,res) =>{
        res.send({name,version})
            })
    reserva(v1)    
    user(v1) 
    carro(v1)
    
    estacionamento(v1) 
    app.use('/v1', v1)
    test(v2)
  app.use('/v2', v2)


}