const carroController = require('../Controllers/carro.Controller')
const middleware = require('../Utils/middleware')
const security = require('../Utils/security')

module.exports = (router) =>{
    //get routes
    
    router.route('/carro/:id').get(middleware.verifyJWT,(req,res,next) =>{
        carroController.getcarroById(req,res,next)
    }) 
    
    router.route('/carro').get(middleware.verifyJWT,(req,res,next) =>{
        carroController.getAllCarro(req,res,next)
    }) 
    //post routes
    router.route('/carro/update').put(middleware.verifyJWT,security.checkBodyCarroUpdate,(req,res,next) =>{
      carroController.update(req,res,next)
    })
    router.route('/carro/del').delete(middleware.verifyJWT,(req,res,next) =>{
        carroController.delcarroById(req,res,next)
    })
    router.route('/carro/insert').post(middleware.verifyJWT,security.checkBodyCarro,(req,res,next) =>{
        carroController.insert(req,res,next)
    })
    router.route('/carro/select').post(middleware.verifyJWT,(req,res,next) =>{
        carroController.select(req,res,next)
    })
    
}

