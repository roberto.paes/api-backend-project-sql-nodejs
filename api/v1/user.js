const userController = require('../Controllers/user.Controller')
const middleware = require('../Utils/middleware')
const Joi = require('joi');
const security = require('../Utils/security')
module.exports = (router) =>{
  //get routes

    router.route('/usuario').get(middleware.verifyJWT,(req,res,next) =>{
        userController.getUserById(req,res,next)
   })           
   
//post routes

router.route('/usuario/select').post(middleware.verifyJWT,(req,res,next) =>{
  userController.select(req,res,next)
})
router.route('/usuario/login').post(  middleware.validarDTO('body', {
  senha: Joi.string().required().messages({
    'any.required': `"senha" é um campo obrigatório`,
    'string.empty': `"senha" não deve ser vazio`,
    'string.min': `"senha" não deve ter menos que {#limit} caracteres`,
  }),
  username: Joi.string().messages({
    'any.required': `"usuario" é um campo obrigatório`,
    'string.empty': `"usuario" não deve ser vazio`,
  }),
  email: Joi.string().email().messages({
    'any.required': `"email" é um campo obrigatório`,
    'string.empty': `"email" não deve ser vazio`,
  }),
}),(req,res,next) =>{
  console.log('request login')
  userController.login(req,res,next)
})
     router.route('/usuario/update').put(middleware.verifyJWT,(req,res,next) =>{
      userController.update(req,res,next)
  })
  router.route('/usuario/del').delete(middleware.verifyJWT,(req,res,next) =>{
      userController.delUserById(req,res,next)
  })
     router.route('/usuario/insert').post(security.checkBodyUsuario,(req,res,next) =>{
      userController.insert(req,res,next)
  })


    }