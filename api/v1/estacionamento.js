const estacionamentoController = require('./../Controllers/estacionamento.Controller')
const middleware = require('../Utils/middleware')
const security = require('../Utils/security')

module.exports = (router) => {
    //public routes

    router.route('/estacionamento/lista').get((req, res, next) => {
        estacionamentoController.estacionamentoLista(req, res, next)
    })
    router.route('/estacionamento/lista/:estado/:cidade?/:bairro?').get((req, res, next) => {
        estacionamentoController.getEstByBairroAndEstado(req, res, next)
    })
    router.route('/estacionamento/get/all').get((req, res, next) => {
        estacionamentoController.getAll(req, res, next)
    })
    router.route('/estacionamento/:id').get((req, res, next) => {
        estacionamentoController.getestById(req, res, next)
    })
    
    router.route('/estacionamento').get(middleware.verifyJWT,(req, res, next) => {
        middleware.verifyTipo(req,res,next,[estacionamentoController.getAllEstUser,1])
    })
    router.route('/estacionamento/update').put(middleware.verifyJWT,security.checkBodyEstacionamentoUpdate,middleware.validCep,(req, res, next) => {
        middleware.verifyTipo(req,res,next,[estacionamentoController.update,1])
        estacionamentoController.update(req, res, next)
    })
    router.route('/estacionamento/del').delete(middleware.verifyJWT,(req, res, next) => {
        middleware.verifyTipo(req,res,next,[estacionamentoController.delEstById,1])
    })
    router.route('/estacionamento/insert').post(middleware.verifyJWT,security.checkBodyEstacionamento,middleware.validCep,(req, res, next) => {
        middleware.verifyTipo(req,res,next,[estacionamentoController.insert,1])
    })
    // router.route('/estacionamento/search/:column/:query').get(middleware.verifyJWT, (req, res, next) => {
    //     estacionamentoController.search(req, res, next)
    // })
    router.route('/estacionamento/search/distance/:id').get((req, res, next) => {
        estacionamentoController.searchDistance(req, res, next)
    })
}