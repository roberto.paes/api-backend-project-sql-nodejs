const reservaTipoZeroController = require('../Controllers/reserva/reserva.Controller')
const middleware = require('../Utils/middleware')
const security = require('../Utils/security')

module.exports = (router) =>{
  //get routes
  router.route('/reserva/:id').get(middleware.verifyJWT,(req,res,next) =>{
    reservaTipoZeroController.getresById(req,res,next)
  })
  router.route('/reserva').get(middleware.verifyJWT,(req,res,next) =>{
    reservaTipoZeroController.getAllres(req,res,next)
  })           
  router.route('/reserva/select').post(middleware.verifyJWT,security.checkBodyReserva,(req,res,next) =>{
    reservaTipoZeroController.select(req,res,next)
  })
  router.route('/reserva/update').put(middleware.verifyJWT,security.checkBodyReservaUpdate ,(req,res,next) =>{
    reservaTipoZeroController.update(req,res,next)
  })
  router.route('/reserva/del').delete(middleware.verifyJWT,security.checkBodyReserva,(req,res,next) =>{ 
    reservaTipoZeroController.delresById(req,res,next)
  })
  router.route('/reserva/insert').post(middleware.verifyJWT,security.checkBodyReserva,(req,res,next) =>{
    reservaTipoZeroController.insert(req,res,next)
  })
}