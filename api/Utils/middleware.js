const jwt = require('jsonwebtoken');
const Joi = require('joi');
const cepPromise = require('cep-promise')


const criarDetalhes = (error) => {

    return error.details.reduce((acc, item) => {
  
      console.log(acc);
  
      console.log(item);
  
      return [
        ...acc, item.message
      ];
  
    }, []);
  
  }
  
exports.verifyJWT = function(req, res, next){
    const token = req.headers['x-access-token'];
    if (!token) return res.status(401).json({ auth: false, message: 'No token provided.' });
    
    jwt.verify(token, process.env.SECRET, function(err, decoded) {
      if (err) return res.status(500).json({ auth: false, message: 'Failed to authenticate token.' });
      
   //set user tipo and userid to routes
      req.userTipo = decoded.tipo;
      req.userId = decoded.id;
      console.log('user',req.userTipo, req.userId)
      next();
    });
}
//func req,res,next
exports.verifyTipo = async function(req,res,next,...func){

  //console.log(func)
  func.forEach(async n=> {
    if(req.userTipo == n[1]){
        await n[0](req,res,next)
        next();
    }else{
      res.status(401).json({error:'this route requires privilegies'})
    }
  })
}
exports.validarDTO = (type, params) => {


    return (req, res, next) => {
  
      const schema = Joi.object().keys(params);
  
      const { value, error } = schema.validate(req[type], {
        allowUnknown: false,
      });
  
      req[type] = value;
  
      return error ? res.status(422).send({
        error: [...criarDetalhes(error)],
      }) : next();
  
    }
  
  }
  
  exports.validCep = async (req,res,next) => {

var request = req.body.update ? req.body.update : req.body
await cepPromise(request.cep,{ providers: ['brasilapi'] })
    .then((item) =>{
      if(item.state == request.estado && item.city == request.cidade && item.street == request.logradouro && item.neighborhood == request.bairro){
        next()
        }else{
          res.status(404).json({error: ['cep inválido']})
        }
    })
  
  }
  