const jwt = require('jsonwebtoken');
const bodyCheck = ['usuarioId']
const middleware = require('../Utils/middleware')
const Joi = require('joi')
const reserva = {
        id: Joi.number(),
        nome: Joi.string(),
        preco: Joi.number(),
        entrada: Joi.date().messages({
                'any.required': `Data de entrada é necessária.`,
        }),
        saida: Joi.date().disallow(Joi.ref('entrada')).messages({
                'any.required': `Data de saida é necessária.`,
                'any.invalid': `Data de entrada e saída não podem ser as mesmas.`
        }),
        
        carroId: Joi.number(),
        status: Joi.number(),
        estacionamentoId: Joi.number(),
}
const reservaUpdater = {
        nome: Joi.string(),
        status: Joi.number()
        
}
const estaciomamentoSchema = Joi.object({
        tempo: Joi.alternatives(Joi.number().min(1).required(),Joi.string()),
        preco: Joi.number().required(),
        id: Joi.number().required()
}).required();
const finder = {
        id: Joi.number()
}
const estacionamentoUpdater = {
        nome: Joi.string(),
        cep: Joi.number(),
        numero: Joi.number(),
        vagas: Joi.number(),
        logradouro: Joi.string(),
        complemento: Joi.string().allow('').optional(),
        bairro: Joi.string(),
        tarifa:Joi.array().items(estaciomamentoSchema).min(1).unique(),
        cidade: Joi.string(),
        estado: Joi.string().max(2)
}
const estacionamento = {
        id: Joi.number(),
        nome: Joi.string().required().messages({
                'any.required': `Nome é obrigatório.`,
        }),
        cep: Joi.number().required().messages({
                'any.required': `Cep é obrigatório.`,
        }),
        numero: Joi.number(),
        vagas: Joi.number().required().messages({
                'any.required': `O número de vagas é obrigatório.`,
        }),
        logradouro: Joi.string().required().messages({
                'any.required': `O logradouro é obrigatório.`,
        }),
        complemento: Joi.string().optional(),
        bairro: Joi.string().required().messages({
                'any.required': `O bairro  é obrigatório.`,
        }),
        tarifa:Joi.array().items(estaciomamentoSchema).min(1).unique().required().messages({
                'any.required': `A tarifa é obrigatória.`,
        }),
        cidade: Joi.string().required().messages({
                'any.required': `A cidade é obrigatória.`,
        }),
        estado: Joi.string().max(2).required().messages({
                'any.required': `O estado é obrigatório.`,
        })
}
const usuario = {
        id: Joi.number(),
        nome: Joi.string(),
        username: Joi.string(),
        email: Joi.string().email(),
        datanascimento: Joi.date(),
        tipo: Joi.number(),
        senha: Joi.string(),
        cpf: Joi.string()//Joi.string().regex(/^\d{3}\.\d{3}\.\d{3}\-\d{2}$/).required()
}
const carro = {
        id: Joi.number(),
        marca: Joi.string().regex(/^([a-zA-Z0-9]\s*)+$/).messages({
                'any.required': `O campo marca é requerido.`,
                'string.base': `O campo marca deve ser um texto.'`,
                "string.pattern.base": "O campo marca deve ser um texto."
        }),
        modelo: Joi.string().regex(/^([a-zA-Z0-9]\s*)+$/).messages({
                'any.required': `O campo modelo é requerido.`,
                'string.base': `O campo modelo deve ser um texto.'`,
                "string.pattern.base": "O campo modelo deve ser um texto."
        }),
        placa: Joi.string().regex(/[A-Z]{3}[0-9][0-9A-Z][0-9]{2}/).messages({
                'any.required': `O campo placa é requerido.`,
                'string.base': `O campo placa deve ser em formato ABC-1234.'`,
                "string.pattern.base": "O campo placa deve ser em formato ABC-1234."
        })
}
exports.checkBodyReservaUpdate = middleware.validarDTO('body', {
        find: Joi.object().keys(finder),
        update: Joi.object().keys(reservaUpdater),
        
})
exports.checkBodyReserva = middleware.validarDTO('body', {
        ...reserva
})
exports.checkBodyCarro = middleware.validarDTO('body', {
     ...carro
})
exports.checkBodyCarroUpdate = middleware.validarDTO('body', {
        find: Joi.object().keys(finder),
        update: Joi.object().keys(carro)
        
})
exports.checkBodyUsuario = middleware.validarDTO('body', {
        find: Joi.object().keys(usuario),
        update: Joi.object().keys(usuario),
        ...usuario
})
exports.checkBodyEstacionamentoUpdate = middleware.validarDTO('body', {
        find: Joi.object().keys(finder),
        update: Joi.object().keys(estacionamentoUpdater),
        
})
exports.checkBodyEstacionamento= middleware.validarDTO('body', {
        ...estacionamento
})


