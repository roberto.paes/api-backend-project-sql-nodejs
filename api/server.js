require('dotenv').config();

const express = require('express')

const app = express()
const cors = require('cors')
var bodyParser = require('body-parser')
var database = require('./../db/database')
const router = require('./router')

const PORT = process.env.PORT;
const HOST = process.env.HOST;

app.use(express.urlencoded({extended: true}));

app.use(express.json());
app.use(cors());    

database.estacionamento = require('./Models/testacionamento')(database.sequelize,database.Sequelize)
database.reserva = require('./Models/treserva')(database.sequelize,database.Sequelize)
database.user = require('./Models/tuser')(database.sequelize,database.Sequelize)
database.carro = require('./Models/tcarro')(database.sequelize,database.Sequelize)
database.cargo = require('./Models/tcargo')(database.sequelize,database.Sequelize)
database.tarifa = require('./Models/ttarifa')(database.sequelize,database.Sequelize)

database.user.hasMany(database.carro);
database.user.hasMany(database.reserva);
database.user.hasMany(database.estacionamento);

database.user.belongsTo(database.cargo,{ foreignKey: 'tipo'});
database.tarifa.belongsTo(database.estacionamento)
database.tarifa.belongsTo(database.estacionamento)
database.reserva.belongsTo(database.carro);
database.estacionamento.belongsTo(database.user);
database.reserva.belongsTo(database.user);
database.reserva.belongsTo(database.estacionamento);
database.carro.belongsTo(database.user);

database.estacionamento.hasOne(database.tarifa)
database.estacionamento.hasMany(database.reserva)


database.carro.hasMany(database.reserva);


router(app)



app.listen(PORT, function () {
  // database.sequelize.sync({ force: true }).then(() => {
  //   console.log("Drop and re-sync db.");
   
  // })
    console.log(`Listening on ${PORT}!`)
  })