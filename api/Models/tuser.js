const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {

const User = sequelize.define('usuario', {

    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    username: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    senha: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    nome: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    cpf: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    datanascimento: {
            type: DataTypes.DATEONLY,
            allowNull: true
   },
    email: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    tipo: {
        type: DataTypes.INTEGER,
        allowNull: false
    }

})
User.beforeCreate((user, options) => {

    return bcrypt.hash(user.senha, 3)
    .then(hash => {
    user.senha = hash;
    })
    .catch(err => {
    throw new Error();
    });
    });
 return User
}
