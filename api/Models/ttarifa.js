
module.exports = (sequelize, DataTypes) => {

    const Tarifa = sequelize.define('tarifa', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
          },
          tarifa: {
              type: DataTypes.JSON,
              allowNull: false
          },   
    }, {timestamps: false, 

    // If don't want createdAt
    createdAt: false,
  
    // If don't want updatedAt
    updatedAt: false})
    
    return Tarifa
    }