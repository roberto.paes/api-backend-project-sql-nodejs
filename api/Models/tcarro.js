
module.exports = (sequelize, DataTypes) => {

const Carro = sequelize.define('carro', {

    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    marca: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    modelo: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    placa: {
        type: DataTypes.TEXT,
        allowNull: false
    }
   
  
})

return Carro
}