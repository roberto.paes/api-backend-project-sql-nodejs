
module.exports = (sequelize, DataTypes) => {

    const Cargo = sequelize.define('cargo', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: false,
            allowNull: false,
            primaryKey: true
          },
          descricao: {
            type: DataTypes.STRING,
            allowNull: false
          },
          cargo: {
            type: DataTypes.STRING,
            allowNull: false
          },   
    }, {timestamps: false, 

    // If don't want createdAt
    createdAt: false,
  
    // If don't want updatedAt
    updatedAt: false})
    
    return Cargo
    }