const estacionamento = require("../v1/estacionamento")

 module.exports = (sequelize, DataTypes) => {

const Estacionamento = sequelize.define('estacionamento', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    vagas: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    nome: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cep: {
        type: DataTypes.STRING,
        allowNull: false
    },
    vaga_ocupada: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    logradouro: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      numero: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      complemento: {
        type: DataTypes.STRING,
        allowNull: true
      },
      bairro: {
        type: DataTypes.STRING,
        allowNull: true
      },
      cidade: {
        type: DataTypes.STRING,
        allowNull: true
      },
      estado: {
        type: DataTypes.STRING(2),
        allowNull: true
      }

})
 
return Estacionamento;
 }