module.exports = (sequelize, DataTypes) => {

var Reserva = sequelize.define('reserva', {

    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    preco: {
        type: DataTypes.DECIMAL(8, 2),
        allowNull: true
    },
    nome: {
        type: DataTypes.TEXT,
        defaultValue: "",
        allowNull: true
    },
    entrada: {
        type: DataTypes.DATE,
        allowNull: true
    },
    
    saida: {
        type: DataTypes.DATE,
        allowNull: true
    },
    status: {
        type: DataTypes.INTEGER,
        allowNull: true
    },

})

return Reserva
}