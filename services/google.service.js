const axios = require('axios'); 


module.exports.googleDistanceMatrix = async (origin,ceps) => {
    var cep = ''
    ceps.map((item)=>{
        var newcep = item.cep.replace(/(\d{5})(\d{3})/, '$1-$2');
        cep += newcep + '|'
    })
    cep = cep.slice(0, -1) 
    
    console.log(process.env.GOOGLE_KEY)
    return await axios.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=place_id:${origin}&destinations=${cep}&mode=driving&language=pt-BR&sensor=false&key=${process.env.GOOGLE_KEY}`)
    
}

module.exports.googletGetInfoById = async (id) => {
    var rua 
    var estado 
    var cidade

    const response =  await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?place_id=${id}&key=${process.env.GOOGLE_KEY}`)
    response.data['results'][0]['address_components'].map((item,index)=>{
        switch(item.types[0]){
        case 'administrative_area_level_1':
        estado = item.short_name
        break
        case 'administrative_area_level_2':
        cidade = item.long_name
        break
        case 'route':
        rua = item.long_name
        break
        }
        })
        return {rua,estado,cidade}
    
}