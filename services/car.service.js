const database = require('../db/database')

const buscarPorCarro = async (placa) => {
    return database.carro.findOne({
      where: {
        placa
      },
    });
  }
  
  module.exports = {
    buscarPorCarro
  }