const database = require('../db/database')

const buscarPorEmail = async (email) => {
    return database.user.findOne({
      where: {
        email: email
      },
    });
  }
  const buscarPorUsuario = async (usuario) => {
    return database.user.findOne({
      where: {
        username: usuario
      },
    });
  }
  const buscarPorCpf = async (cpf) => {
    return database.user.findOne({
      where: {
        cpf
      },
    });
  }
  

module.exports = {
    buscarPorEmail,
    buscarPorCpf,
    buscarPorUsuario

}