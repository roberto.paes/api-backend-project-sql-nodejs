# api-backend-estacionamento-sql-nodejs

api-backend-estacionamento-sql-nodejs

Estacionamento

| Rota | TIPO | Função |
| ------ | ------ |  ------ |
| https://backend-api-mysql.herokuapp.com/v1/estacionamento/update |PUT| atualizar estacionamento |
| https://backend-api-mysql.herokuapp.com/v1/estacionamento/del |DELETE|  deletar estacionamento |
| https://backend-api-mysql.herokuapp.com/v1/estacionamento/insert |POST| inserir estacionamento |
| https://backend-api-mysql.herokuapp.com/v1/estacionamento |GET| mostrar lista de estacionamentos |
| https://backend-api-mysql.herokuapp.com/v1/estacionamento/id/#id |GET|  mostrar estacionamento com um id específico |
| https://backend-api-mysql.herokuapp.com/v1/estacionamento/bairro/#bairro |GET| mostrar estacionamentos por bairro |
| https://backend-api-mysql.herokuapp.com/v1/estacionamento/bairro |GET| mostrar todos os bairros cadastrados | 
| https://backend-api-mysql.herokuapp.com/v1/estacionamento/search/#columntext/#searchtext |GET| Executa uma procura por um valor específico em uma coluna | 

Reserva

| Rota | TIPO | Função |
| ------ | ------ |  ------ |
| https://backend-api-mysql.herokuapp.com/v1/reserva/update |PUT| atualizar reserva |
| https://backend-api-mysql.herokuapp.com/v1/reserva/del |DELETE| deletar reserva |
| https://backend-api-mysql.herokuapp.com/v1/reserva/insert |POST| inserir reserva |
| https://backend-api-mysql.herokuapp.com/v1/reserva |GET|mostrar lista de reservas |
| https://backend-api-mysql.herokuapp.com/v1/reserva/id/#id |GET| mostrar reserva com um id específico |
| https://backend-api-mysql.herokuapp.com/v1/reserva/select |POST| selecionar reservas por chaves específicas |

Usuarios

| Rota | TIPO | Função |
| ------ | ------ |  ------ |
| https://backend-api-mysql.herokuapp.com/v1/usuario/update |PUT| atualizar usuario |
| https://backend-api-mysql.herokuapp.com/v1/usuario/del |DEL| deletar usuario |
| https://backend-api-mysql.herokuapp.com/v1/usuario/insert |POST| inserir usuario |
| https://backend-api-mysql.herokuapp.com/v1/usuario |GET|mostrar lista de usuarios |
| https://backend-api-mysql.herokuapp.com/v1/usuario/id/#id|GET| mostrar usuario com um id específico |
| https://backend-api-mysql.herokuapp.com/v1/usuario/select|POST| selecionar usuarios por chaves específicas |
| https://backend-api-mysql.herokuapp.com/v1/usuario/login|POST| rota para autenticar usuários |

Carro

| Rota | TIPO | Função |
| ------ | ------ |  ------ |
| https://backend-api-mysql.herokuapp.com/v1/carro/update |PUT| atualizar carro |
| https://backend-api-mysql.herokuapp.com/v1/carro/del |DELETE| deletar carro |
| https://backend-api-mysql.herokuapp.com/v1/carro/insert |POST| inserir carro |
| https://backend-api-mysql.herokuapp.com/v1/carro | GET |mostrar lista de carro |
| https://backend-api-mysql.herokuapp.com/v1/carro/id/#id | GET | mostrar carro com um id específico |
| https://backend-api-mysql.herokuapp.com/v1/carro/select | POST| selecionar carro por chaves específicas |





